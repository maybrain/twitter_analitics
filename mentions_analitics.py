import csv
import pprint
stats = {}
with open('tweets.csv','rb') as f:
	reader = csv.reader(f)
	for row in reader:
		tweet = row[5]
		arroba = tweet.find('@')
		if arroba != -1: 
			pre_user = tweet[ arroba:tweet.find(' ',arroba) ]
			try:
				if pre_user[-1] not in (':',','): user = pre_user
				else: user = pre_user [:-1]
				if user in stats:
					stats[user] = stats[user] + 1
				else:
					stats[user]=1
			except IndexError: pass

for key, value in sorted(stats.iteritems(), key=lambda(k,v):(v,k), reverse=True):
	print "%s:%s" % (key,value)