# twitter_analitics

Some personal Python scripting tools intended to be used with the "tweets.csv" obtained at the "Get Your File" on Twitter Settings -> Account

First of all, these were developed on Python 2.7.10

- **mentions_analitics.py**: With "tweets.csv" on the same folder of the script, returns an output with a list ordered by the number of ocurrences an username mention exists in the file. Output can be redirected to another csv file: For example: `python mentions_analitics.py > mentions.csv`
- **mentions_users.py**: With "tweets.csv" on the same folder of the script, asks interactivily for a username, and returns, on screen, the list of tweets in which that username appears and the total number of hits for that username. Usage: `python mentions_users.py`

The main skeleton is, basically, the same: open the file, load it on memory and traverse: 
- In mentions, it gets all substring containing an `@` ending on space or comma and adding a new key to a dictionary with value 0 if the key is new or adding 1 to the value if the key was already loaded on the dictionary. Finally, the dict object is ordered by the value criteria and pretty printed to the output.
- In users, it's a loop breaked on getting 'EXIT' on the input for an username (with @). If it's not an EXIT, there traverse the object and prints the tweet, its ID and adds 1 to a total counter, which it's printed at the end of the loop.
 
Personal work, no roadmap of improvements, neither bug fixing or else. Use, adapt and improve as you wish.